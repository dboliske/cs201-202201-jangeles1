package exams.first;

import java.util.Scanner;

public class ExamQ2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter an integer:");
		int val = Integer.parseInt(input.nextLine());
		if (val % 2 == 0 && val % 3 == 0) {
			System.out.println("foobar");
		} else if (val % 2 == 0) {
			System.out.println("foo");
		} else if (val % 3 == 0) {
			System.out.println("bar");
		} else {
			System.out.println("");
		}
		input.close();

	}

}
