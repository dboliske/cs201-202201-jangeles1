package exams.first;

import java.util.Scanner;

public class ExamQ4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// set an array for user input of 5 words
		String word[]= new String[5];
		for (int i =0; i<word.length;i++) {
			System.out.println("Enter a word: ");
			word[i]=input.nextLine();
		}
		
		String comparison = word[0];
		for (int i=0;i<word.length;i++) {
			// equalsIgnoreCase used to compare the strings to look for a match (case sensitive)
			// have to loop through array to find any word that appears more than once
			if (word[i].equalsIgnoreCase(comparison)) {
				comparison = word[i];
			} 
		// print out the word that appears more than once
		System.out.println(comparison);
		
	}
		input.close();
}
}

