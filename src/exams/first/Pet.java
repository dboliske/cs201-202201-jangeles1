package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = "Peppa";
		setAge(age);
	}
	
	public Pet(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age > 0) {
			this.age = age;
		} else {
			this.age=1;
		}
	}
	
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Object obj) {
		if (name != (((Pet) obj).getName())) {
			return false;
		} else if (age != ((Pet) obj).getAge()) {
			return false;
		}
		return true;
		}
	
	public String toString() {
		return name + " is " + age + " years old";
	}
	
	
	
	
	
}
