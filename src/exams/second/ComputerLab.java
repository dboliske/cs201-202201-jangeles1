/* Jessica Angeles
 * 04/29/2022
 * Final Q1: ComputerLab subclass
 */
package exams.second;

public class ComputerLab extends Classroom {
	
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = false;
	}
	
	public ComputerLab(String building, String roomNumber, int seats, boolean computer) {
		super(building, roomNumber, seats);
		computers = computer;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}
	
	public String toString() {
		return super.toString() + "\nComputers Available: " + (computers ? "Yes" : "No");
	}

}
