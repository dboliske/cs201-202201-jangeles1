/* Jessica Angeles
 * 04/29/2022
 * Final Q5: Jump Search Algorithm
 */
package exams.second;

import java.util.Scanner;

public class JumpSearch {

	public static void main(String[] args) {
		
		double[] numbers = new double[] {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		Scanner input = new Scanner(System.in);
		System.out.println("Search for a number: ");
		double value = Double.parseDouble(input.nextLine());
		int position = jumpSearch(numbers, value);
		if (position == -1) {
			System.out.println("-1"); // index is not present
		} else {
			System.out.println(value + " is found at position " + position); // prints the position of where the number is found
		}
		input.close();
	}

	public static int jumpSearch(double[] numbers, double value) {
		int jump = (int) Math.sqrt(numbers.length); 
		int start = jump - 1;
		return jump(numbers, value, jump, start); // recursive
		
		/* Ignore: Iterative method
		while (search[Math.min(jump, search.length) - 1] < value) {
			start = jump;
			jump += (int) Math.sqrt(search.length);
			if (start >= search.length) {
				return -1;
			}
		}
		while (search[start] < value) {
			start++;
			if (start == Math.min(jump, search.length)) {
				return -1;
			}
		}
		
		if (search[start] == value) {
			return start;
		}
		*/
 }

	public static int jump(double[] numbers, double value, int jump, int start) {
		if(start < numbers.length && value > numbers[start]) {
			start += jump;
			return jump(numbers, value, jump, start); // recursive
		} else {
			for (int i = start - jump + 1; i <= start && i < numbers.length; i++) {
				if(value == numbers[i]) {
					return i;
				} 
			}
		}
		return -1;
}
}
