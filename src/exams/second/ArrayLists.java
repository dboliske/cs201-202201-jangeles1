/* Jessica Angeles
 * 04/29/2022
 * Final Q3: Array Lists Program
 */
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		ArrayList<String> num = new ArrayList<String>();
		
		boolean done = false;
		do {
			System.out.println("Enter a number: ");
			String val = input.nextLine();
			num.add(val);
			if (val.equalsIgnoreCase("done")) {
				done = true;
			}	
		} while (!done);
		
		/* Testing String ArrayList Print: 
		for (int i = 0; i < num.size() - 1; i++) {
			System.out.println(num.get(i));
		}
		*/
		
		ArrayList <Integer> values = new ArrayList<Integer>();
		for (int i = 0; i < num.size() - 1; i++) {
			values.add(Integer.parseInt(num.get(i)));
			// Testing Int ArrayList Print: System.out.println(values.get(i));
		}
		
		int min = values.get(0);
		for (int i = 0; i < num.size() - 1; i++) {
			if(values.get(i) < min) {
				min = values.get(i);
			}
		}
		
		int max = values.get(0);
		for (int i = 0; i < num.size() - 1; i++) {
			if(values.get(i) > max) {
				max = values.get(i);
			}
		}
		
		System.out.println("Minimum value entered: " + min);
		System.out.println("Maximum value entered: " + max);
		
		input.close();
	
	}

}
