/* Jessica Angeles
 * 04/29/2022
 * Final Q1: Classroom superclass
 */
package exams.second;

public class Classroom {

	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "Stuart";
		roomNumber = "112E";
		seats = 30;
	}
	
	public Classroom(String building, String roomNumber, int seats) {
		this.building = building;
		this.roomNumber = roomNumber;
		setSeats(seats);
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public void setRoomNumber(String number) {
		this.roomNumber = number;
	}

	public void setSeats(int seats) {
		if (seats > 0 && seats != 0) {
			this.seats = seats;
		}
	}
	
	public String getBuilding() {
		return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}

	public String toString() {
		return "Building: " + building + ", " + "Room: " + roomNumber + ", " + "Seats Available: " + seats;
	}

}
