/* Jessica Angeles
 * 04/29/2022
 * Final Q4: Selection Sort Algorithm
 */
package exams.second;

public class Sorting {

	public static void main(String[] args) {
		
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		words = selectionSort(words);
		for (int i = 0; i < words.length; i++) {
			System.out.println(words[i]);
		}
	}

	public static String[] selectionSort(String[] sort) {
		for (int i = 0; i < sort.length; i++) {
			int first = i;
			for (int j = i + 1; j < sort.length; j++) {
				if(sort[j].compareToIgnoreCase(sort[first]) < 0) {
					first = j;
				}
			}
			
			if (first != i) {
				String temp = sort[i];
				sort[i] = sort[first];
				sort[first] = temp;
			}
		}
		return sort;
	}

}
