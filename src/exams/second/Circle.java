/* Jessica Angeles
 * 04/29/2022
 * Exam Q2: Circle subclass
 */
package exams.second;

public class Circle extends Polygon {

	private double radius;
	
	public Circle() {
		super();
		radius = 1.00;
	}
	
	public Circle(String name, double radius) {
		this.name = name;
		setRadius(radius);
	}
	
	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		} else {
			radius = 1.00;
		}
	}
	
	public double getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		return "Radius: " + radius;
	}
	
	@Override
	public double area() {
		return Math.PI * radius * radius;
	}

	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}
}
