/* Jessica Angeles
 * 04/29/2022
 * Final Q2: Polygon abstract superclass
 */
package exams.second;

public abstract class Polygon {
	
	protected String name;
	
	public Polygon() {
		name = "Rectangle";
	}
	
	public Polygon(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	
	public abstract double area();
	public abstract double perimeter();
	
	

}
