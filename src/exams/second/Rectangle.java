/* Jessica Angeles
 * 04/29/2022
 * Final Q2: Rectangle subclass
 */
package exams.second;

public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	public Rectangle() {
		super();
		width = 1.00;
		height = 1.00;
	}
	
	public Rectangle(String name, double width, double height) {
		this.name = name;
		setWidth(width);
		setHeight(height);
	}
	
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		} else {
			width = 1.00;
		}
	}

	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		} else {
			height = 1.00;
		}
	}

	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	@Override
	public String toString() {
		return "Width: " + width + ", " + "Height: " + height;
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}
}
