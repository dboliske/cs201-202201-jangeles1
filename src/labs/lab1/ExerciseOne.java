// Jessica Angeles
// CS 201- Section 01
// 01-20-2022
// Exercise 1: Reading input and writing output

package labs.lab1;

import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a Scanner object that will read user input
		String name; 							// declare a variable that will save the input
		System.out.print("Enter a name: ");		// prompt a user for a name
		name = input.nextLine();				// completes reading user input and assigns user input to the variable name
		System.out.println("Echo: " + name);	// echo the name to the console
		input.close();							// close Scanner object
	}

}
