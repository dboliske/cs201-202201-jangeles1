/*Jessica Angeles
 * CS 201- Section 01
 * 01-21-2022
 * Exercise 4: Temperature Program
 */
package labs.lab1;

import java.util.Scanner;

public class ExerciseFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);							// create Scanner object 
		int tempF;														// create variable tempF (int data type) for temperature in Fahrenheit
		System.out.print("Enter a temperature in Fahrenheit: ");		// prompt the user for a temperature in Fahrenheit
		tempF = input.nextInt();										// assigns user input to variable tempF
		double tempC;													// create variable tempC (double float point type) for temperature in Celsius
		tempC =  (tempF-32)*0.5556;										// operation to convert user input temperature in Fahrenheit to Celsius
		System.out.println("The temperature in Celsius is: "+ tempC);	// display result to the console

		int tempCelsius; 														 // create variable tempCelsius (int data type) for temperature in Celsius
		System.out.print("Enter a temperature in Celsius: "); 					 // prompt the user for a temperature in Celsius
		tempCelsius = input.nextInt(); 											 // assigns user input to variable tempCelsius
		double tempFahrenheit; 													 // create variable tempFahrenheit (double float point type) for temperature in Fahrenheit
		tempFahrenheit = (tempCelsius*1.8)+32; 									 // operation to convert user input temperature in Celsius to Fahrenheit
		System.out.println("The temperature in Farenheit is: "+ tempFahrenheit); // display result to the console
		input.close(); // close scanner
	}

}

/* Test Table
 * Temperature Range:			Input			Expected			Actual
 * Low							10F				-12.2222C			-12.2232C	
 * 								10C				50F					50.0F
 * Middle						50F				10C					10.0008C
 * 								50C				122F				122.0F
 * High							100F			37.7778C			37.7808C
 * 								100C			212F				212.0F
 */

// The test table shows that the program works as expected.