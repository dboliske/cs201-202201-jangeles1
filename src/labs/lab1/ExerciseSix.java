/* Jessica Angeles
 * CS 201- Section 01
 * 01-21-2022 
 * Exercise 6: Inches to Centimeters Conversion Program 
 */

package labs.lab1;

import java.util.Scanner;

public class ExerciseSix {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);								//create Scanner object
		int inches;															//create variable inches (int data type) for inches
		System.out.print("Enter a value for inches: ");  					//prompt user to input a value for inches
		inches = input.nextInt();											//assign user input to variable inches								
		double centimeters = inches * 2.54;									// convert inches to centimeters by multiplying by 2.54
		System.out.println("Inches to centimeters: "+ centimeters + " cm"); // display result to the console
		input.close();														//close Scanner

	}

}
/* Test Table
 * Dimensions			Input			Expected			Actual
 * Small				5 in			12.7 cm				12.7 cm
 * Middle			    25 in			63.5 cm				63.5 cm				
 * Big					200 in			508 cm				508.0 cm									
 */

// The test table shows that the program works as expected.
