# Lab 1

## Total

19/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   1/1
* Exercise 5
  * Program     1/2
  * Test Plan   1/1
* Exercise 6
  * Program     2/2
  * Test Plan   1/1
* Documentation 5/5

## Comments
No points off, but for Exercise 5, inputs of less than 12 inchs resulted in 0.0 sq ft. so remember to use doubles for extra precision where needed
-1 for Exercise 5, you have the wrong formula for the surface area of the wooden box, should've 2* ((l*w) + (w*d) + (l*w))