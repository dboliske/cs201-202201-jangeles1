// Jessica Angeles
// CS 201- Section 01
// 01-20-2022
// Exercise 2: Performing Arithmetic Calculations

package labs.lab1;

public class ExerciseTwo {

	public static void main(String[] args) {
		int myAge = 19;															 // declare a variable for my age and assign the value 19 to it providing the int data type
		int fatherAge = 46;														 // declare a variable for my father's age and assign the value 46 to it providing the int data type
		int age;																 // declare a variable age that will store the operation 
		age = fatherAge - myAge;												 // Operation: my age subtracted from my father's age and storing that in the variable age
		System.out.println("My age subtracted from my father's age is:  " + age);// output operation to the console
		
		int birthYr = 2002;													// declare a variable for my birth year and assign the value 2002 to it providing the int data type
		int birthYrx2;														// declare a variable for my birth year multiplied by 2 that will store the operation 
		birthYrx2 = birthYr*2;												// Operation: my birth year multiplied by 2 and storing that in the variable birthYrx2
		System.out.println("My birth year multipied by 2 is: " + birthYrx2);// output operation to the console on a new line
		
		int heightInches = 62; 																	// declare a variable for my height in inches and assign the value 62 providing the int data type
		double conversionInCm = 2.54;															// declare a variable for the conversion of inches to centimeters & assign the value 2.54 using the double float point type
		double heightInCm;																		// declare a variable heightInCm that will store operation
		heightInCm = heightInches * conversionInCm;												// Operation: convert my height in inches to centimeters
		System.out.println("My height converted from inches to centimeters is: " + heightInCm); // output operation to the console on a new line
		
		int myheightIn = 62;																				// declare a variable for my height in inches and assign the value 62 providing the int data type				
		int conversionInFt = 12;																			// declare a variable for the conversion of inches to feet & assign the value 12 using the int data type
		int heightFt;																						// declare variables heightFt and heightIn that will store operations
		int heightIn;
		heightFt = myheightIn / conversionInFt;																// Operation: convert my height in inches to feet					
		heightIn = myheightIn % conversionInFt;																// Operation: include remaining inches from the conversion
		System.out.println("My height in inches to feet and inches is: "+ heightFt + "'"+ heightIn + '"');  // output operation to the console on a new line
		
	}

}
