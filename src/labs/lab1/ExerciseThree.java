// Jessica Angeles
// CS 201- Section 01
// 01-20-2022
// Exercise 3: Getting a char from keyboard input

package labs.lab1;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 					// create a Scanner object that will read user input
		String firstname; 											// declare a variable that will save the input 
		System.out.print("Enter a first name: ");					// prompt a user for a first name
		firstname = input.nextLine();								// completes reading user input & assigns user input to the variable firstname
		System.out.println("First Initial: " + firstname.charAt(0));// displays the user's first initial to the screen 
		input.close();												// close Scanner object

	}

}
