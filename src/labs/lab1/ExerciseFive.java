/*Jessica Angeles
 * CS 201- Section 01
 * 01-21-2022
 * Exercise 5: Box and Wood Program
 */
package labs.lab1;

import java.util.Scanner;

public class ExerciseFive {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);					//create Scanner object
	int length;												//create variable length (int data type) for length
	System.out.print("Enter length in inches of a box: ");  //prompt user to input length
	length = input.nextInt();								//assign user input to variable length
	int width;												//create variable width (int data type) for width
	System.out.print("Enter width in inches of a box: ");   //prompt user to input width
	width = input.nextInt();								//assign user input to variable width
	int depth;												//create variable depth (int data type) for depth
	System.out.print("Enter depth in inches of a box: ");	//prompt user to input depth
	depth = input.nextInt();								//assign user input to variable depth
	float amountWood = (length/12) * (width/12);			// convert length and width to feet and multiply length by width to calculate square feet to find the amount of wood needed
	System.out.println("The amount of wood needed to make the box is " + amountWood + " square feet, and the box has a depth of " + depth + " inches."); // output result to the console
	input.close();	//close Scanner

	}

}

/* Test Table
 * Dimensions					Input			Expected			Actual
 * Small														
 * 	Length						24 in			
 * 	Width						24 in
 * 	Depth						24 in
 * Square feet									4 sq.ft				4.0 square feet
 * Middle
 * 	Length						48 in
 * 	Width						48 in
 * 	Depth						48 in
 * Square feet									16 sq.ft			16.0 square feet								
 * Big	
 * 	Length						144 in
 * 	Width						144 in
 * 	Depth						144 in
 * Square feet									144 sq.ft			144.0 square feet								
 */

// The test table shows that the program works as expected.