// Jessica Angeles
// CS 201- Section 01
// 01-19-2022
// Correct TryVariables.java & run

package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		short small = 25; // error fix: use primitive short for variable small
		byte tiny = 19;

		float f = .0925F; // error fix: include semicolon at the end of line of code
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small); // error fix: Capitalize the "s" in "system"
		System.out.println("tiny is " + tiny); // error fix: variable tiny was misspelled as "tine"
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal); // error fix: should be a period instead of a comma between "out" and "println"
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character); // error fix: should include "+" between string and variable name
		System.out.println("t is " + t);

	}

}