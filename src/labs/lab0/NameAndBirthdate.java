// Jessica Angeles
// CS 201- Section 01
// 01-16-2022
// Name and Birthdate program
package labs.lab0;

import java.util.Scanner;

public class NameAndBirthdate {

	public static void main(String[] args) {
		// create scanner for user input
		Scanner input = new Scanner(System.in);
		// prompt the user to input first name 
	    System.out.print("Enter first and last name: ");
	    String name = input.nextLine();
	    // prompt the user to input birth month
	    System.out.print("Enter birth month (use abbreviation): ");
	    String month = input.nextLine();
	    // prompt the user to input birth day
	    System.out.print("Enter birth day: ");
	    String day = input.nextLine();
	    // prompt the user to input birth year
	    System.out.print("Enter birth year: ");
	    String year = input.nextLine();
	    // print out name and birth date in the following format:
	    System.out.print("My name is "+name+ " and my birthdate is " + month + ". " + day + ", " +year + ".");
	    // close Scanner
	    input.close();
	}

}
