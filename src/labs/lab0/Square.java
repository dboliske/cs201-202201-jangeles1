// Jessica Angeles
// CS 201- Section 01
// 01-16-2022
// Square Program

// Pseudocode
// Step 1: Create Scanner to read in user input
// Step 2: Create character array with character 'a'
// Step 3: Print out character array 4 times to create a 4x4 square,... 
// (use println to write arrays in separate lines)
// Step 4: Close Scanner


package labs.lab0;

import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		// create scanner for user input
		Scanner input = new Scanner(System.in);
		// create character array
		char array []  = {'a','a','a','a'};
		// print character array
		System.out.println(array);
		System.out.println(array);
		System.out.println(array);
		System.out.println(array);
		// close Scanner
		input.close();
	}

}

// The output result was what was expected because the console printed out a square of the character 'a'.
