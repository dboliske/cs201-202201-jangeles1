/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-18-2022
 * PhoneNumber Application Class
*/
package labs.lab4;

public class PhoneNumberApplicationClass {

	public static void main(String[] args) {
		// new application class for PhoneNumber
		PhoneNumber pn1 = new PhoneNumber();
		// instantiates PhoneNumber using the default constructor
		pn1.toString();
		// prints out default constructor
		System.out.println(pn1);
		// instantiates PhoneNumber using the non-default constructor
		pn1.setCountryCode("1");
		pn1.setAreaCode("708");
		pn1.setNumber("2222222");
		// prints out non-default constructor
		System.out.println(pn1.toString());
	}

}
