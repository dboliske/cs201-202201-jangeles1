/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-21-2022
 * Exercise 1: GeoLocation
*/
package labs.lab4;

public class GeoLocation {
	// create two instance variables, lat and lng. Type: double
	private double lat;
	private double lng;
	
	// write default constructor (arbitrary, should be valid)
	public GeoLocation () {
		lat = 0.0;
		lng = 0.0;
	}
	
	// write non-default constructor (takes in values for lat and lng)
	public GeoLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
	// write 2 accessor methods, one for each instance variable (public: accessible, return type: type of instance variable)
	public double getLat() {
		return lat;
	}
	public double getLng ()	{
		return lng;
	}
	// write 2 mutator methods, one for each instance variable (start with set, take in one attribute, public, return type: void)
	public void setLat (double lat) {
		this.lat = lat;
	}
	public void setLng (double lng) {
		this.lng = lng;
	}
	// write a method that will return the location in the format "(lat,lng)" (the toString method)
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	// write a method that will return true if latitude is between -90 and +90
	public boolean validLat(double lat) {
		if (lat >= -90 && lat <= 90) {
			return true;
		} else
			return false;
	}
	// write a method that will return true if longitude is between -180 and +180
	public boolean validLng(double lng) {
		if (lng >= -180 && lng <= 180) {
			return true;
		} else
			return false;
	}
	// write a method that will compare this instance to another GeoLocation g (the equals method)
	public boolean equals(GeoLocation g) {
		if (this.lat != g.getLat()) {
			return false;
		} else if (this.lng != g.getLng()) {
			return false;
		} else
			return true;
	}
}
