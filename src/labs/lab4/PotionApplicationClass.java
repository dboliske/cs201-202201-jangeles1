/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-18-2022
 * Potion Application Class
*/

package labs.lab4;

public class PotionApplicationClass {

	public static void main(String[] args) {
		// new application class for Potion
		Potion p1 = new Potion();
		// instantiates Potion using the default constructor
		p1.toString();
		// prints default constructor
		System.out.println(p1);
		// instantiates Potion using the non-default constructor
		p1.setName("Tom");
		p1.setStrength(10.0);
		// prints out non-default constructor
		System.out.println(p1.toString());
	}

}
