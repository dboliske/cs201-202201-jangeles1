/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-18-2022
 * GeoLocation Application Class
*/
package labs.lab4;

public class GeoLocationApplicationClass {

	public static void main(String[] args) {
		// new application class for GeoLocation
		GeoLocation g1 = new GeoLocation();
		// instantiates GeoLocation using the default constructor
		g1.toString();
		// prints default constructor
		System.out.println(g1);
		// instantiates GeoLocation using the non-default constructor
		GeoLocation g2 = new GeoLocation();
		g2.setLat(80);
		g2.setLng(100);
		// prints non-default constructor
		System.out.println(g2);
		
	}

}
