/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-21-2022
 * Exercise 3: Potion
*/
package labs.lab4;

public class Potion {
	// create two instance variables name (type:string) and strength (type:double)
	private String name;
	private double strength;
	
	// write the default constructor
	public Potion () {
		name = "Jerry";
		strength = 50.0;
	}
	
	// write the non-default constructor
	public Potion (String name, double strength) {
		this.name = name;
		this.strength = strength;
	}
	
	// write two accessor methods, one for each instance variable
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	// write two mutator methods, one for each instance variable
	public void setName (String name) {
		this.name = name;
	}
	
	public void setStrength (double strength) {
		this.strength = strength;
	}
	
	// write a method that will return the entire string as a single string (the toString method)
	public String toString() {
		return "My name is " + name + " and my strength is " + strength + ".";
	}
	
	// write a method that will return true if the strength is between 0 and 10
	public boolean validStrength(double strength) {
		if (strength >= 0 && strength <= 10) {
			return true;
		} else 
			return false;
	}
	
	// write a method that will compare this instance to another Potion (the equals method)
	public boolean equals(Potion p) {
		if (this.name != p.getName()) {
			return false;
		} else if (this.strength != p.getStrength()) {
			return false;
		} else
			return true;
	}
}
