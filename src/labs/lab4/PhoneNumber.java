/* Jessica Angeles 
 * CS 201 - Section 01
 * 02-21-2022
 * Exercise 2: PhoneNumber
*/
package labs.lab4;

public class PhoneNumber {
	// create three instance variables: countryCode, areaCode, and number (type:String)
	private String countryCode;
	private String areaCode;
	private String number;
	
	// write the default constructor
	public PhoneNumber () {
		countryCode = "1";
		areaCode = "773";
		number = "1235678";
	}
	// write the non-default constructor
	public PhoneNumber (String cCode, String aCode, String number) {
		this.countryCode = cCode;
		this.areaCode = aCode;
		this.number = number;
	}
	// write 3 accessor methods, one for each instance variable
	public String getCountryCode() {
		return countryCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public String getNumber() {
		return number;
	}
	// write 3 mutator methods, one for each instance variable
	public void setCountryCode (String cCode) {
		this.countryCode = cCode;
	}
	public void setAreaCode (String aCode) {
		this.areaCode = aCode;
	}
	public void setNumber (String number) {
		this.number = number;
	}
	// write a method that will return the entire phone number as a single string (the toString method)
	public String toString() {
		return "+" + countryCode + " " + "(" + areaCode + ")" + " " + number.substring(0,3) + "-" + number.substring(3);
		
	}
	// write a method that will return true if the areaCode is 3 characters long
	public boolean validAreaCode(String aCode) {
		if (aCode.length() == 3) {
			return true;
		} else 
			return false;
	}
	
	// write a method that will return true if the number is 7 characters long
	public boolean validNumber (String number) {
		if (number.length() == 7) {
			return true;
		} else
			return false;
	}
	// write a method that will compare this instance to another PhoneNumber (the equals method)
	public boolean equals(PhoneNumber pn) {
		if (this.countryCode != pn.getCountryCode()) {
			return false;
		} else if (this.areaCode != pn.getAreaCode()) {
			return false;
		} else if (this.number != pn.getNumber()) {
			return false;
		} else 
			return true;
	}
}
