/* Jessica Angeles
 * CS 201-01 
 * 03-31-2022
 * Exercise Four: Binary Search Algorithm
 */
package labs.lab7;
import java.util.Scanner;
public class ExerciseFour {
	
	// Main Method
	public static void main(String[] args) {
	String[] word = {"c", "html", "java", "python", "ruby", "scala"}; // create array of Strings
	Scanner input = new Scanner(System.in);	// create Scanner object
	System.out.print("Enter a term to search for: ");	// prompt user for specific value
	String in = input.nextLine();	// store user input value
	int index = binSearch(word, in);	// implement binary search algorithm
	if(index == -1) { // array out of bounds
		System.out.println(in + "was not found.");
	} else {	// print the term searched and the index of the term
		System.out.println(in + " is at index " + index + " of the array.");
	}
	input.close();	// close Scanner object
	}
	
	// Binary Search Algorithm
	public static int binSearch(String[] word, String in) {
		// initialize beginning, end, and position variables; initialize flag
		int beg = 0;	
		int end = word.length;
		int position = -1;
		boolean found = false;
		
		// while loop to iterate through String array
		while(!found && 0 != word.length) {
			int middle = (beg+end)/2; // intialize middle of array
			// finds index of the term if it is in the middle of the array
			if(word[middle].equalsIgnoreCase(in)) {
				found = true;
				position = middle;
			// finds index of the term respective to the beginning and middle terms
			} else if(word[middle].compareToIgnoreCase(in) < 0) {
				beg = middle + 1;
			// finds the index of the final term 
			} else {
				end = middle;
			}
			
		} 
		
		return position;	// returns term position
	}

}
