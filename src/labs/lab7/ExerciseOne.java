/* Jessica Angeles
 * CS 201-01 
 * 03-31-2022
 * Exercise One: Bubble Sort Algorithm
 */
package labs.lab7;

public class ExerciseOne {
	// Main Method
	public static void main(String[] args) {
		int[] intArray = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};	// create an int array to store the values given 
		
		for (int i : bubbleSort(intArray)) {	// loop through the integers after the bubble sort algorithm has been applied 
			System.out.print(i + " ");	// print sorted integers
		}
	}
	
	// Bubble Sort Algorithm
	public static int[] bubbleSort(int[] intArray) {
		boolean done = false;	// initalize flag to false
		
		do {	// do while loop to loop through entire array until it is sorted
			done = true;
			for(int i = 0; i < intArray.length - 1; i++) {	// loops through int array
				if(intArray[i] > intArray[i+1]) {	// loop structure to rearrange integers if the int in position i is greater than the int in position i+1
					int temp = intArray[i];	// temp used to store the value if is greater
					intArray[i]=intArray[i+1]; 
					intArray[i+1] = temp;	// order of ints is rearranged
					done = false;
				}
			}
		} while (!done);
		
		return intArray;	// return statement for array of integers
	}

}
