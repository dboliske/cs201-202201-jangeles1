/* Jessica Angeles
 * CS 201-01
 * 03-31-2022
 * Exercise Three: Selection Sort
 */
package labs.lab7;

public class ExerciseThree {
	// Main Method
	public static void main(String[] args) {
		double[] val = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};	// create array of doubles
		for (double i : selcSort(val)) {	//iterate through sorted array of doubles and print to console
			System.out.print(i + " ");
		}

	}
	
	// Selection Sort Algorithm
	private static double[] selcSort(double[] val) {
		
		for (int i = val.length - 1; i > 0; i--) { // iterate through length of array of doubles to check for values that are not in order
			int first = 0;
			for(int j = 1; j <= i; j++) {
				if(val[j] > val[first]) {
					first = j;
				}
			}
			double temp = val[first]; // sort array of doubles
			val[first]= val[i];
			val[i] = temp;
		}
		return val; // return sorted array of doubles
	}
}



