/* Jessica Angeles
	CS 201-01
	03-31-2022
	Exercise Two: Insertion Sort Algorithm
*/
package labs.lab7;

public class ExerciseTwo {
	// Main Method
	public static void main(String[] args) {
		String[] word = {"cat","fat","dog","apple","bat","egg"};	// create String array of words
		word = inSort(word); // implement insertion sort algorithm
		for(String i : word) {	//iterate through implementation of insertion sort and print out sorted words
			System.out.print(i+ " ");
		}
	}

	// Insertion Sort Algorithm
	private static String[] inSort(String[] word) {
		for(int i = 1; i < word.length; i++) { // start iteration through String array at index 1
			while(i > 0 && word[i].compareTo(word[i-1]) < 0){	// compares word in array 
				String temp = word[i];	// temp storage of word that is misplaced
				word[i] = word[i-1];	
				word[i-1] = temp;	// sort word
				i--;	// decrement index position respective to the array
			}
		}
		return word;
	}

}
