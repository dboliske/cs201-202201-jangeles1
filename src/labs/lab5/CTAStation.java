/* Jessica Angeles 
 * CS 201 - Section 01
 * 03-07-2022
 * CTAStation
*/
package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStation extends GeoLocation {
	// create variables
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	// create default constructor
	public CTAStation() {
		name = "Harlem";
		location = "elevated"; 
		wheelchair = true;
		open = true;
	}
	// create non-default constructor
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat,lng);
		this.name=name;
		this.location=location;
		this.wheelchair=wheelchair;
		this.open=open;
			
	}
	
	// accessor for name variable
	public String getName() {
		return name;
	}
	
	// accessor for location variable
	public String getLocation() {
		return location;
	}
	
	// accessor for wheelchair variable
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	// accessor for open variable
	public boolean isOpen() {
		return open;
	}
	
	// mutator for name variable
	public void setName(String name) {
		this.name = name;
	}
	
	// mutator for location variable
	public void setLocation(String location) {
		this.location = location;
	}
	
	// mutator for wheelchair variable
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	// mutator for open variable
	public void setOpen(boolean open) {
		this.open = open;
	}
	// toString to return name, lat, lng, locaton, wheelchair, and open variables to console
	@Override
	public String toString() {
		return name + "," + super.toString() + "," + location + "," + wheelchair + "," + open;
	}
	
	// equals method used to compare Object parameter to intance of CTAStation
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)){
			return false;
	} else if (!(obj instanceof CTAStation)) {
		return false;
	}
		
	CTAStation c = (CTAStation)obj;
	if (this.name != c.getName()) {
		return false;
	} else if (this.location != c.getLocation()) {
		return false;
	} else if (this.wheelchair != c.hasWheelchair()) {
		return false;
	} else if (this.open != c.isOpen()) {
		return false;
	} 
	 return true;
}
}
