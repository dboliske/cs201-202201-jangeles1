# Lab 5
 
## Total

19/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        2/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     1/2
- Exits                         1/1

## Comments
For your default constructor, rememeber to also call the super() default constructor as well.

Your displayNearest method only reads in the user's input but it doesn't calculate or output the near station