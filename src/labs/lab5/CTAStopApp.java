/* Jessica Angeles 
 * CS 201 - Section 01
 * 03-07-2022
 * CTAStopApp
*/
package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create Scanner object
		CTAStation[] data = readFile("src/labs/lab5/CTAStops.csv"); // calls readfile to load the data
		data = menu(input, data); // passes data to menu
		input.close(); // close Scanner object
	}
	// read CTAStops file
	private static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[36];
		int count = 0;
		try {
			File f = new File("src/labs/lab5/CTAStops.csv");
			Scanner input = new Scanner(f);
			// reads stations from input file CTAStops.csv
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(","); 
					// stores the data in an instance of CTAStation s
					CTAStation s = new CTAStation(values[0], 
							Double.parseDouble(values[1]),
							Double.parseDouble(values[2]),
							values[3],
							Boolean.parseBoolean(values[4]),
							Boolean.parseBoolean(values[5]));
					stations[count] = s;
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
			// catches exceptions 
		} catch (FileNotFoundException fnf) {
			System.out.println("file not found");
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		 	  
		return stations;
	}
	
	// menu
	private static CTAStation[] menu(Scanner input, CTAStation[] data) {
		boolean done = false;
		
		//displays menu options
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // displays station names
					data = displayStationNames(data);
					break;
				case "2": // displays stations with/without wheelchair access
					data = displayByWheelchair(data, input);
					break;
				case "3": // displays nearest station
					data = displayNearest(data,input);
					break;
				case "4": // exits
					done = true;
					break;
				default: // continues to prompt until user enters a valid choice
					System.out.println("That is not a valid option. Please try again.");
			}
		} while (!done);
		
		return data;
	}
	// displays stations and their wheelchair access
    private static CTAStation[] displayByWheelchair(CTAStation[] data, Scanner input) {
    	// create arrays for storage of data
    	CTAStation[] stations = new CTAStation[36];
    	String[] wc = new String[36];
    	String[] names = new String[36];
    	Scanner user = new Scanner(System.in);
    	int count = 0;
		try {
			File f = new File("src/labs/lab5/CTAStops.csv");
			input = new Scanner(f);
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					wc[count] = values[4];
					names[count]=values[0];
					count++;
				} catch (Exception e) {
					
				}
			}
		}  catch(Exception e) {
		}
		// initialize boolean variables
		boolean wheelchair = false;
		boolean done = false;
		// continues to prompt for accessibility until user selects an appropriate choice
		do {
			// prompts the user accessibility ('y' or 'n')
			System.out.println("Accessibility (y/n):");
			String yn = user.nextLine();
			switch (yn.toLowerCase()) {
			// wheelchair access 
			case "y":
			case "yes":
				wheelchair = true;
				done = true;
				// loops through array to look at stations that have wheelchair access and prints them out
				for (int i=1; i<wc.length-1; i++) {
					if (wc[i].equals("TRUE")) {
						System.out.println(names[i]+ " station has wheelchair access." );
					}
				}
				break;
			// no wheelchair access
			case "n":
			case "no":
				wheelchair = false;
				done = true;
				// loops through array to look at stations that do not have wheelchair access and prints them out
				for (int i=1; i<wc.length-1; i++) {
					if (wc[i].equals("FALSE")) {
						System.out.println(names[i] + " station does not have wheelchair access.");	
					}
				}
				break;
			
			default:
				System.out.println("Please try again."); // prompts user to select another option
			}
			
		} while (!done);
		
		return data;
	}
    // displays names of stations
	private static CTAStation[] displayStationNames(CTAStation[] data) {
		CTAStation[] stations = new CTAStation[36];
		int count = 0;
		String[] n = new String[36];
		try {
			File f = new File("src/labs/lab5/CTAStops.csv");
			Scanner input = new Scanner(f);
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(","); 
					// parses CTAStops.csv file
					CTAStation s = new CTAStation(values[0], 
							Double.parseDouble(values[1]),
							Double.parseDouble(values[2]),
							values[3],
							Boolean.parseBoolean(values[4]),
							Boolean.parseBoolean(values[5]));
					stations[count] = s;
					n[count] = values[0];
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
		} catch(Exception e) {
		}
		// iterates through CTAStation and prints out the name of the stations
		for (int i =0;i<stations.length-2;i++) {
			System.out.println(n[i]);
		}
		
		return data;
	}
	// displays nearest station to user 
	private static CTAStation[] displayNearest(CTAStation[] data, Scanner input) {
		// prompts the user for a latitude and longitude
		System.out.println("Enter a value for latitude: ");
		double lat2 = Double.parseDouble(input.nextLine());
		System.out.println("Enter a value for longitude: ");
		double lng2 = Double.parseDouble(input.nextLine());
		// returns data
		return data;
	}
}
