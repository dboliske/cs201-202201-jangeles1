/* Jessica Angeles
 * CS 201-Section 1
 * 02-08-2022
 * Exercise 2:Saving User Input Into A File Program
 */
package labs.lab3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
		// initialize variables for value, number, and count
		String value;
		int number = 0;
		int count = 0;
		// start flag 
		boolean running = true;
		// create Scanner object
		Scanner input = new Scanner(System.in);
		// while loop to repeat prompt as long as flag is true
		while (running)	{
			// prompt user for number
			System.out.println("Number:");
			// assigns user input to string value
			value = input.nextLine();
			// updates count of how many entries the user has made
			count++;
			// conditional statement that keeps the while loop running unless the user enters "done"
			if (!value.equalsIgnoreCase("Done")) {
				number = Integer.parseInt(value);
			} else {
				running = false;		
			}	
			}
		// creates int array of user input the size of the number of entries made 
		int[] nums=new int[count-1];
		// create Scanner object for file
		Scanner user = new Scanner(System.in);
		// prompts user for a file name
		System.out.print("File name: ");
		String filename = user.nextLine();
		user.close();
		// creates a file with the user input name
		File fObj = new File(filename);
		// try-catch for file errors
		try {
			// writes to the file based on the int array of user input
			FileWriter f = new FileWriter("src/labs/lab3/"+fObj);
			
			for (int i=0; i<count-1; i++) {
				nums[i]=number;
				f.write(nums[i] + "\n");
			}
			f.flush();
			f.close();
			// gets message in case of an IO exception
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
			
		// prints the user input array
		for (int i=0;i<count-1;i++) {
		System.out.println(nums[i]);
		    }
		// closes scanner
		input.close();
		
	}
}

	
	 		
	





