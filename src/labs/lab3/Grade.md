# Lab 3

## Total

19/20

## Break Down
 
* Exercise 1    6/6
* Exercise 2    5/6
* Exercise 3    6/6
* Documentation 2/2

## Comments
Exercise 2: 
	No points off - try to prompt user with more instructions so they know to enter Done to finish reading
	-1 - Line 55 replaces all the numbers in your array with the last entered number so your output file only shows only the last number in x lines. I think without L55, it would work fine. 