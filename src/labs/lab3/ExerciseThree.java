/* Jessica Angeles
 * CS 201-Section 1
 * 02-02-2022
 * Exercise 3: Minimum Value From Array Program
 */
package labs.lab3;

public class ExerciseThree {

	public static void main(String[] args) {
		// create int array of values provided
		int[] values = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		// Initialize minimum value to be the first value in the array
		int min = values[0];
		// for loop to loop through the array to check which value is the minimum
		for (int i=1; i<values.length;i++) {
			// updates minimum 
			if (values[i]< min) {
				min = values[i];
				// prints out minimum value of the array to the console
				System.out.println("The minimum value of the array is: " + min);
			}
		
		}
		

	}

}
