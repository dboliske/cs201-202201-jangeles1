/* Jessica Angeles
 * CS 201-Section 01
 * 02-02-2022
 * Exercise 1: Average Grade From File Program
 */
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) throws IOException {
		// create File object for grades.csv
		File f = new File("src/labs/lab3/grades.csv");
		// create Scanner object
		Scanner input = new Scanner(f);
		// create int array that store 14 values for grades
		int [] grades = new int[14];
		// create & declare int variable count = 0 
		int count = 0;
		// while loop to continue reading if there is another line in the input
		while (input.hasNextLine()) {
			// read user input
			String line = input.nextLine();
			// create array that stores values separated by a comma
			String[] values = line.split(",");
			// obtain grades from csv file and stores it into the array grades
			grades[count]=Integer.parseInt(values[1]);
			// update count
			count++;
		}
		// close Scanner
		input.close();
		// create variable of type double and initialize "total" to 0
		double total = 0;
		// for loop that updates the "total" by adding the grades together 
		for (int i=0;i<grades.length;i++) {
			total = total +grades[i];
		}
		// print out the class average
		System.out.println("Average Class Grade = " + (total/grades.length));
	}

}
