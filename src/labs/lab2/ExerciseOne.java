/*Jessica Angeles
 * CS 201-Section 01
 * 01-27-2022
 * Exercise 1: Prompt Square Program
 */
package labs.lab2;

import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) {
		// creates Scanner object
		Scanner input = new Scanner (System.in); 
		// prompts user for the number used to determine the dimensions of the square
		System.out.println("Enter a number: "); 
		// declaring string variable from user input
		String number = input.nextLine(); 
		// converts string into int 
		int dimension = Integer.parseInt(number); 
		// nested for loop initiated at 1, set with the conditions that the row/column is less than or equal to the dimension value
		for (int row = 1; row <= dimension; row++)	{ 
			for (int column = 1; column <= dimension; column++)	{ 
				// prints star character
				System.out.print("* "); 
			}
			// creates a new row
			System.out.println(""); 
		}
		// closes scanner object
		input.close();
	}

}