/*Jessica Angeles
 * CS 201: Section 01
 * 01-27-2022
 * Exercise 3: Menu and Option Program
 */
package labs.lab2;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		// create scanner object
		Scanner input = new Scanner(System.in);
		// initialize flag 
		boolean running = true;
		// do while loop to display menu 
		do {
			System.out.println("1.Say Hello");
			System.out.println("2.Addition");
			System.out.println("3.Mulitplication");
			System.out.println("4.Exit");
			System.out.println("Choice:");
			String choice = input.nextLine();
			// switch case statements based on user menu choice
			switch (choice) {
			// case 1 prints hello statement
			case "1":
				// prints hello to the console
				System.out.println("Hello");
				break;
			// case 2 prompts user for two numbers and returns the sum 
			case "2":
				// prompts for first number
				System.out.println("Enter a number: ");
				// stores user input in num1 variable
				String num1 = input.nextLine();
				// converts string to int
				int number1 = Integer.parseInt(num1);
				// prompts for second number
				System.out.println("Enter a second number: ");
				// stores user input in num2 variable
				String num2 = input.nextLine();
				// converts string to int
				int number2 = Integer.parseInt(num2);
				// adds two numbers
				int addition = number1+number2;
				// displays sum to console
				System.out.println("The sum is: " +addition);
				break;
			// case 3 prompts the user for two numbers and returns the product
			case "3":
				// prompts for first number
				System.out.println("Enter a number: ");
				// stores user input in val1 variable
				String val1 = input.nextLine();
				// converts string to int
				int value1 = Integer.parseInt(val1);
				// prompts for second number
				System.out.println("Enter a second number: ");
				// stores user input in val2 variable
				String val2 = input.nextLine();
				// converts string to int
				int value2 = Integer.parseInt(val2);
				// multiplies two numbers
				int multiplication = value1*value2;
				// displays product to console
				System.out.println("The product is: " +multiplication);
				break;
			// case 4 allows the user to exit the program
			case "4":
				// ends flag
				running = false;
				break;
			default:
				// default option: in case the user enters an invalid choice
					System.out.println("That is not an option");
			}
		} while (running);
		// closes scanner
		input.close();
		// displays a message that the user chose to exit
		System.out.println("User left progam");
	}

}
