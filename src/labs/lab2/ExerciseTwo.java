/*Jessica Angeles
 * CS 201-Section 01
 * 01-27-2022
 * Exercise 2: Average Grade Program
 */

/* Test Table
 * Input			 	Expected Average	Actual Average
 * 77, 2, 16			31.666666666667     31.666666666666668	
 * 21, 89, 32, 97		59.75				59.75
 * 83, 93, 69, 19, 56  	64					64.0
 * 
 * Program works
 */
package labs.lab2;

import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
		// declares string and int variables for grades, sum, and count 
		String value;
		int number;
		int sum = 0;
		int count = 0;
		// creates scanner object
		Scanner input = new Scanner(System.in);
		// initialize flag
		boolean running = true;
		// while loop to keep prompting for grade as long as the user does not enter -1
		while (running)	{
			// prompts user for grade
			System.out.println("Grade (Enter -1 when done):");
			// stores user input in value variable
			value = input.nextLine();
			// converts string to int
			number = Integer.parseInt(value);
			// updates the sum and count variables
			sum = number + sum;
			count++;
			// if loop structure to stop asking user for input 
			if (number == -1) {
				// end flag
				running = false;
				// update sum
				sum++;
				// calculate average
				double avg = ((double)sum)/((double)(count-1));
				// print average to console
				System.out.println("The average is: " + avg);
			}
	}	// close scanner
		input.close();
}
}
