/* Jessica Angeles
 * CS 201-Section 01
 * 03-11-2022
 * Deli Queue Application
 */
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Deli {
	// create instance variable for customer name
	private String name;
	
	// write default constructor 
	public Deli() {}
	
	// write non-default constructor 
	public Deli(String name) {
		this.name=name;
	}
	
	// write accessor method for customer name
	public String getName() {
		return name;
	}
	
	// write mutator method for customer name
	public void setName(String name) {
		this.name = name;
	}
	
	// write a method that will return customer name in a string 
	public String toString() {
		return name;
	}
	
	// create menu for Deli Queue Application
	public static ArrayList<Deli> menu(Scanner input, ArrayList<Deli> data){
		// intiates boolean do while loop to loop through options
		boolean done = false;
		// menu options
		String[] menuOptions = {"Add customer to queue", "Help customer", "Exit"};
		do {
			// prints menu options in number format
			for (int i=0; i<menuOptions.length; i++) {
				System.out.println((i+1) + ". " + menuOptions[i]);
			}
			// prompts user for their choice
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			// switch case based upon user choice
			switch (choice) {
				case "1": // Option: Add customer to queue
					data = addCust(data, input);
					break;
				case "2": // Option: Help customer (remove customer from queue)
					data = removeCust(data, input);
					break;
				case "3": // Option: Exit
					done = true;
					break;
					// default case used to prompt user for a valid choice
				default:
					System.out.println("Please try again.");
			}
		} while (!done);
		return data;
		
	}

	private static ArrayList<Deli> removeCust(ArrayList<Deli> data, Scanner input) {
		// prints out the customer name that will be helped (removed)
		System.out.println("Helping customer " + data.get(0).toString());
		// removes customer from the front of the queue (position 0)
		data.remove(0);
		return data;
	}

	private static ArrayList<Deli> addCust(ArrayList<Deli> data, Scanner input) {
		// prompts the user for a name
		System.out.print("Customer name: ");
		String name = input.nextLine();
		Deli object = new Deli(name);
		// adds user input name to the end of the ArrayList of customers
		data.add(object);
		// returns the customers position in the queue
		System.out.println("Position in the queue: " + data.size());
		return data;
	}
	
	
	public static void main(String[]args) {
		// create Scanner object
		Scanner input = new Scanner(System.in);
		// create ArrayList for Deli Queue Application
		ArrayList<Deli> data = new ArrayList<Deli>();
		// menu
		data = menu(input, data);
		// close Scanner object
		input.close();
		// prints goodbye message to user
		System.out.println("Goodbye!");
	}


}
