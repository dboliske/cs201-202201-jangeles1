# Final Project

## Total

143/150

## Break Down

Phase 1:                                                            43/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      3/4
  - Describe how you will store the data                            2/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           2/7
- UML Class Diagrams                                                10/10
- Testing Plan                                                      10/10

Phase 2:                                                            100/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     15/15
- Test plan                                                         10/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            10/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments

### Design Comments
When describing 2b, you stated that the input file will be similar to the output file in the sense that it will be the same format. However, I failed to see an explanation on how, after reading the file in, that you would process the raw data of the file and organize it into something that you could use for storing the information -1

When describing 2c, you did a good job at showing what information would be stored, however, you didn't describe how you would store the data. For example would it be stored in an array? an ArrayList? a User-defined Object? -1

2f : Please refer to the README.md file in your project package. You created a very unique design for this project, however, there are instructions that ask you to make it a certain way with certain objects and design structure. -5
### Code Comments
