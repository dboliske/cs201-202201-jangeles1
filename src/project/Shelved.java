/* Jessica Angeles
 * 04/28/2022
 * Description: Class which encapsulates the shelved items
 */

package project;

public class Shelved extends StoreItem{
	// Call default constructor from StoreItem 
	public Shelved() {
		super();
	}
	
	// Pass in instance variables from super constructor (name and price)
	public Shelved(String name, double price) {
		super(name, price);
	}
	
	// Implement equals method to compare Object parameter to an instance of Shelved
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Shelved)) {
			return false;
		}
		
		return true;
	}
	
	// Write toString method to return super toString method 
	@Override
	public String toString() {
		return super.toString();
	}
	
	// Write shelved item information (name and price) in CSV format
	@Override
	public String toCSV() {
		return "Shelved," + super.csvData();
	}
}
