/* Jessica Angeles
 * 04/28/2022
 * Description: Class which encapsulates the produce items 
 */

package project;

public class Produce extends StoreItem {
	
	// Create instance variable for produce item expiration date
	private String expDate;
	
	// Call default super constructor from StoreItem and write default constructor for instance variable (expiration date)
	public Produce() {
		super();
		expDate = "05-01-2022";
	}
	
	// Pass in instance variables from super constructor (name and price) and write non-default constructor for instance variable (expiration date). The instance variable "expDate" is later validated in mutator method.
	public Produce(String name, double price, String expDate) {
		super(name, price);
		this.expDate= expDate;
		setExpDate(expDate);
	}

	// Write mutator method for produce expiration date ensuring that the day, month, and year are valid quantities
	public void setExpDate(String expDate) {
		String[] val = expDate.split("/");
		int day = Integer.parseInt(val[0]);
		int month = Integer.parseInt(val[1]);
		int year = Integer.parseInt(val[2]);
		if (day >= 1 && day <= 31 && month >=1 && month <= 12 && year > 0) {
		this.expDate = expDate;
		}
	}
	
	// Write accessor method for produce expiration date
	public String getExpDate() {
		return expDate;
	}
	
	// Implement equals method to compare Object parameter to an instance of Produce
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Produce)) {
			return false;
		}
		
		Produce p = (Produce)obj;
		if (this.expDate != p.getExpDate()) {
			return false;
		}
		
		return true;
	}
	
	// Write toString method to return super toString method along with the expiration date of the particular produce item
	@Override
	public String toString() {
		return super.toString() + "Best by: " + expDate + "\n";
	}
	
	// Write produce information (name, price, and expiration date) in CSV format
	@Override
	public String toCSV() {
		return "Produce," + super.csvData() + "," + expDate;
	}

}
