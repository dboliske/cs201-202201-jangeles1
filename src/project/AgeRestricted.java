/* Jessica Angeles
 * 04/28/2022
 * Description: Class which encapsulates the age-restricted items
 */
package project;

public class AgeRestricted extends StoreItem {

	// Create instance variable for age of an age-restricted item
	private int age;
	
	// Call default super constructor from StoreItem and write default constructor for instance variable (age)
	public AgeRestricted() {
		super();
		age = 21;
	}
	
	// Pass in instance variables form super constructor (name and price) and write non-default constructor for instance variable (age). The instance variable "age" is later validated in mutator method.
	public AgeRestricted(String name, double price, int age) {
		super(name, price);
		this.age= 21;
		setAge(age);
	}

	// Write mutator method for age restriction ensuring that age is equal to or greater than 16 years of age.
	public void setAge(int age) {
		if (age >= 16) {
			this.age = age;
			}
	}
	
	// Write accessor method for age 
	public int getAge() {
		return age;
	}
	
	// Implement equals method to compare Object parameter to an instance of AgeRestricted 
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestricted)) {
			return false;
		}
		
		AgeRestricted a = (AgeRestricted)obj;
		if (this.age != a.getAge()) {
			return false;
		}
		
		return true;
	}
	
	// Write toString method to return super toString method along with a message displaying the age requirement for purchase.
	@Override
	public String toString() {
		return super.toString() + "Must be " + age + " or older to purchase." + "\n" ;
	}
	
	// Write age-restricted item information (name, price, and age) in CSV format
	@Override
	public String toCSV() {
		return "Age Restricted," + super.csvData() + "," + age;
	}
	
	// Write method to calculate the difference between the user's age and the age required for purchase to verify purchase
	public int calcAge(AgeRestricted age) {
		return Math.abs(this.age-age.getAge());
	}
	
}
