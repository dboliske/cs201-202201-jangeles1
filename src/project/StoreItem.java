/* Jessica Angeles
 * 04/28/2022
 * Description: Class which encapsulates the (general) store stock
 */
package project;

public class StoreItem {
	
	// Create instance variables for item name and price, including a threshold value for the price
	private String name;
	private double price;
	public static double priceThreshold = 0.0001;
	
	// Write default constructor for instance variables (name and  price)
	public StoreItem() {
		this.name = "Eraser";
		this.price = 1.00;
	}
	
	// Write non-default constructor for instance variables (name and price). Instance variable "price" is validated in mutator method.
	public StoreItem(String name, double price) {
		this.name=name;
		// this.price = 1.00;
		setPrice(price); 
	}
	
	// Write accessor method for item name
	public String getName() {
		return name;
	}

	// Write mutator method for item name
	public void setName(String name) {
		this.name = name;
	}

	// Write accessor method for item price
	public double getPrice() {
		return price;
	}

	// Write mutator method for item price
	public void setPrice(double price) {
		if (price > 0.00) {
			this.price = price;
		} 
	}
	
	// Write toString method to return item name and price in an accessible format
	@Override
	public String toString() {
		return name + ","  + " $" + price + "\n";
	}
	
	// Write item information (name and price) in CSV format
	protected String csvData() {
		return name + "," + price;
	}
	
	// Write item information (name and price) in CSV format
	public String toCSV() {
		return "Item," + csvData();
	}
	
	@Override
	// Implement equals method to compare Object parameter to instance of StoreItem
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof StoreItem)) {
			return false;
		}
		
		StoreItem i = (StoreItem)obj;
		if (!name.equals(i.getName())) {
			return false;
		} else if (Math.abs(i.getPrice()- this.price) > priceThreshold) {
			return false;
		}
		return true;
	}
	
	}

