/* Jessica Angeles
 * 04/28/2022
 * Description: Main application class for grocery store 
 */
package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class GroceryStore {
	
	// Write main method to load and read a user input file, display menu, and save user file 
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename=input.nextLine();
		StoreItem[] data = readFile(filename);
		data=menu(input,data);
		saveFile(filename,data);
		input.close();
		System.out.println("Have a nice day! Thanks for shopping here!");
	}

	
	// Write method to write data to an output file when requested by user
	public static void saveFile(String filename, StoreItem[] data) {
		try {
			Scanner input = new Scanner (System.in);
			System.out.print("Would you like to save your data (yes or no)?");
			String choice = input.nextLine();
			if (choice.equalsIgnoreCase("yes")) {
				System.out.print("Save file: ");
				String saveFile = input.nextLine();
				FileWriter writer = new FileWriter(saveFile);
			
				for (int i=0; i<data.length; i++) {
					writer.write(data[i].toCSV() + "\n");
					writer.flush();
			} {
				writer.close();
				input.close();
			}
			
			}
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}
	
	// Write menu options (list stock items, create a new item, sell item, search for an item, modify item, exit program)
	public static StoreItem[] menu(Scanner input, StoreItem[] data) {
		boolean done = false;
		String[] options = {"List Stock", "Create Item", "Buy Item", "Search for Item", "Modify Item", "Exit"};
		do {
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1) + ". " + options[i]);
			}
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // List stock 
					listStock(data);
					break;
				case "2": // Create new item and add to the store stock 
					data = createStoreItem(data,input);
					break;
				case "3": // Sell items and remove from store 
					data= sellStoreItem(data,input);
					break;
				case "4": // Search for item given a name 
					data=searchStoreItem(data,input);
					break;
				case "5": // Modify items in store 
					data = modifyStoreItem(data,input);
					break;
				case "6": // Exit 
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but that is not an option. Please try again.");
					return data;
			}
		} while (!done);
		
		return data;
		}
	
	// Write method to allow user to modify a stock item in terms of its properties (name and price)
	public static StoreItem[] modifyStoreItem(StoreItem[] data, Scanner input) {
		boolean done = false;
		do {
			System.out.println("Which item property would you like to edit?: ");
			String[] options = {"Name", "Price", "Exit"};
				for (int i=0; i<options.length; i++) {
					System.out.println((i+1) + ". " + options[i]);
				}
				System.out.print("Choice: ");
				String choice = input.nextLine();
				switch (choice) {
					case "1": // Name 
						for (int i=0; i<data.length; i++) {
						System.out.println("Current Item Name: " + data[i].getName());
						System.out.print("New Name: ");
						data[i].setName(input.nextLine());
						}
						break;
					case "2": // Price
						for (int i=0; i<data.length; i++) {
							System.out.println("Current Item Name: " + data[i].getName() + "," + " Current Item Price: " + data[i].getPrice());
							System.out.print("New Price: ");
							data[i].setPrice(Double.parseDouble(input.nextLine()));
							}
						
						break;
					case "3": // Exit 
						done = true;
						break;
					default:
						System.out.println("I'm sorry, but that is not an option. Please try again.");
						return data;
				}
			} while (!done);
		return data;
	}

	
			
	// Write method to allow a user to search for a particular item provided an item name
	public static StoreItem[] searchStoreItem(StoreItem[] data, Scanner input) {
		System.out.println("Enter item name: ");
		String name = input.nextLine();
		int index = 0;
		for (int i=0; i<data.length; i++) {
			if(data[i].getName().equalsIgnoreCase(name)) {
				index = i;
				System.out.print(data[i].getName() + " was found at index " + index + "\n");	
			}
			}
		return data;
	}

	// Write method to allow user to buy items and remove them from the store's stock
	public static StoreItem[] sellStoreItem(StoreItem[] data, Scanner input) {
		StoreItem[] temp = new StoreItem[10];
		int count = 0;
		for (int i=0; i<data.length; i++) {
			boolean sell = yesOrno("Buy " + data[i].getName(), input);
			if (!sell) {
				if (temp.length == count) {
					temp = resize(temp, temp.length * 2);
				}
				temp[count] = data[i];
				count++;
			}
		}
		
		temp = resize(temp, count);
		return temp;
		
	}
	
	// Write method to prompt the user for a boolean yes or no response in terms of purchasing an item
	public static boolean yesOrno(String string, Scanner input) {
		System.out.print(string + " (yes/no): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not a valid option. Please try again.");
		}
		
		return result;
	}

	// Write method to allow a user to create a new item and add said item to the store's stock
	public static StoreItem[] createStoreItem(StoreItem[] data, Scanner input) {
		StoreItem s;
		System.out.print("Item name: ");
		String name = input.nextLine();
		System.out.print("Item price: ");
		double price = Double.parseDouble(input.nextLine());
		if (price <= 0.00) {
			System.out.println("That is not a valid price. Redirecting you to the main menu.");
			return data;
		}
		
		System.out.println("Type of Item (Produce, Shelved, or Age Restricted): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) {
			case "produce":
				System.out.print("Enter expiration date (MM/DD/YYYY): ");
				String expDate = input.nextLine();
				s = new Produce(name, price, expDate);
				break;
			case "shelved":
				s = new Shelved(name, price);
				break;
			case "age restricted":
				System.out.print("Enter age restriction: ");
				int age = Integer.parseInt(input.nextLine());
				if (age < 16) {
					System.out.println("That is not a valid age restriction. Redirecting you to the main menu.");
					return data;
				}
				s = new AgeRestricted(name, price, age);
				break;
			default:
				System.out.println("That is not a valid grocery store item type. Redirecting you to the main menu.");
				return data;
		}
		
		data = resize(data, data.length+1);
		data[data.length - 1] = s;
		
		return data;
	}

	// Write method to display a cumulative list of the store's current stock
	public static void listStock(StoreItem[] data) {
		for (int i=0; i<data.length; i++) {
			System.out.print(data[i]);
			}
	}

	// Write method to read and store an input file of data
	public static StoreItem[] readFile(String filename) {
		StoreItem[] items = new StoreItem[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					StoreItem s = null;
					switch (values[0].toLowerCase()) {
						case "produce":
							s = new Produce(
								values[1],
								Double.parseDouble(values[2]),
								values[3]
							);
							break;
						case "shelved":
							s = new Shelved(
									values[1],
									Double.parseDouble(values[2])
								);
							break;
						case "age restricted":
							s = new AgeRestricted(
									values[1],
									Double.parseDouble(values[2]),
									Integer.parseInt(values[3])
								);
							break;
					}
					
					if (items.length == count) {
						items = resize(items, items.length*2);
					}
					
					items[count] = s;
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
		} catch(Exception e) {
			System.out.println("Error occurred reading in the file. Please enter a valid file name.");
		}
		
		items = resize(items, count);
		
		return items;
	}

	// Write method to resize the store stock data list
	public static StoreItem[] resize(StoreItem[] data, int size) {
		StoreItem[] temp = new StoreItem[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	
}
